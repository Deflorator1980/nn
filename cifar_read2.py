import matplotlib.pyplot as plt
import numpy as np
from keras.models import model_from_json
from keras.preprocessing import image

# imp_path = 'plane.jpg'
imp_path = 'horse.png'
img = image.load_img(imp_path, target_size=(32,32))
plt.show()  # doesn't show

x = image.img_to_array(img)
x /= 255
x = np.expand_dims(x, axis=0)

json_file = open("cifar10_model.json", "r")
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights("cifar10_model.h5")
loaded_model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

prediction = loaded_model.predict(x)

classes=['самолет', 'автомобиль', 'птица', 'кот', 'олень', 'собака', 'лягушка', 'лошадь', 'корабль', 'грузовик']

print(classes[np.argmax(prediction)])