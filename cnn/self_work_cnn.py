import gc
import numpy as np
from keras.preprocessing import image
from PIL import Image
from keras.utils import np_utils
from keras.models import model_from_json

# im = Image.open('4.png')
# im_grey = im.convert('L')
# im_array = np.array(im_grey)
# im_array=np.reshape(im_array, (1, 784)).astype('float32')
# x = 255 - im_array
# x /= 255

img_path = '7.png'
img = image.load_img(img_path, target_size=(28, 28), grayscale=True)
x = image.img_to_array(img)
x = 255 - x
x /= 255
x = np.expand_dims(x, axis=0)

# json_file = open('mnist_cnn_model_desktop.json', 'r')
# json_file = open('mnist_siamese_graph.json', 'r')
# json_file = open('mnist_mlp_model.json', 'r')
# json_file = open('mnist_hierarchical_rnn_model_desktop.json', 'r')
# json_file = open('mnist_irnn_model_desktop.json', 'r')
json_file = open('mnist_cnn_model_aws.json', 'r')
# json_file = open('mnist_cnn_model_vscale.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# loaded_model.load_weights("mnist_cnn_model_desktop.h5")
# loaded_model.load_weights("mnist_siamese_graph.h5")
# loaded_model.load_weights("mnist_hierarchical_rnn_model_desktop.h5")
# loaded_model.load_weights("mnist_irnn_model_desktop.h5")
loaded_model.load_weights("mnist_cnn_model_aws.h5")
# loaded_model.load_weights("mnist_cnn_model_vscale.h5")

loaded_model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
prediction = loaded_model.predict(x)
# prediction = np_utils.categorical_probas_to_classes(prediction)
prediction = np.argmax(prediction, axis=1)
print(prediction)
gc.collect()
