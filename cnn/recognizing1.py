
# Theano 0.8 и Keras 1
# The easiest
from PIL import Image

from keras.datasets import mnist
from keras.utils import np_utils
from keras.models import model_from_json
import numpy as np

print("Загружаю сеть из файлов")
# Загружаем данные об архитектуре сети
json_file = open("mnist_model.json", "r")
model_json = json_file.read()
json_file.close()
# Создаем модель
model = model_from_json(model_json)
# Загружаем сохраненные веса в модель
model.load_weights("mnist_model.h5")
print("Загрузка сети завершена")


# Загружаем данные
(X_train, y_train), (X_test, y_test) = mnist.load_data()

# Преобразование размерности изображений
X_train = X_train.reshape(60000, 784)
X_test = X_test.reshape(10000, 784)
# Нормализация данных
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255

# Преобразуем метки в категории
Y_train = np_utils.to_categorical(y_train, 10)
Y_test = np_utils.to_categorical(y_test, 10)

# Компилируем загруженную модель
model.compile(loss="categorical_crossentropy", optimizer="SGD", metrics=["accuracy"])

im = Image.open('4b.png')
im_grey = im.convert('L')
im_array = np.array(im_grey)
im_array=np.reshape(im_array, (1, 784)).astype('float32')

# Инвертируем изображение
x = 255 - im_array
# Нормализуем изображение
x /= 255

# Нейронная сеть предсказывает класс изображения
prediction = model.predict(x)
# Преобразуем ответ из категориального представления в метку класса
prediction = np_utils.categorical_probas_to_classes(prediction)
# Печатаем результат
print(prediction)
