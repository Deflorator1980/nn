import gc
import numpy as np
from PIL import Image
from keras.utils import np_utils
from keras.models import model_from_json
im = Image.open('7.png')
im_grey = im.convert('L')
im_array = np.array(im_grey)
im_array=np.reshape(im_array, (1, 784)).astype('float32')
x = 255 - im_array
x /= 255
# json_file = open('/home/a/Documents/Python/dlpython_course/src/saving_models/mnist_model.json', 'r')
json_file = open('mnist_model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# loaded_model.load_weights("/home/a/Documents/Python/dlpython_course/src/saving_models/mnist_model.h5")
# loaded_model.load_weights("./src/saving_models/mnist_model.h5")
loaded_model.load_weights("mnist_model.h5")
prediction = loaded_model.predict(x)
# prediction = np_utils.categorical_probas_to_classes(prediction)
prediction = np.argmax(prediction, axis=1)
print(prediction)
gc.collect()