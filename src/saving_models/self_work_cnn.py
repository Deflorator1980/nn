import gc
import numpy as np
from keras.preprocessing import image
from PIL import Image
from keras.utils import np_utils
from keras.models import model_from_json

# im = Image.open('0.png')
# im_grey = im.convert('L')
# im_array = np.array(im_grey)
# im_array=np.reshape(im_array, (1, 784)).astype('float32')
# x = 255 - im_array
# x /= 255

# img_path = '2.png'
img_path = '7b.png' # todo 7b & 77 get '2' by mnist_cnn_model.json & mnist_cnn_model.h5  ; create $ save modle in mnist_cnn.py
img = image.load_img(img_path, target_size=(28, 28), grayscale=True)
x = image.img_to_array(img)
x = 255 - x
x /= 255
x = np.expand_dims(x, axis=0)

# json_file = open('mnist_cnn_model_desktop.json', 'r')
json_file = open('mnist_cnn_model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# loaded_model.load_weights("mnist_model_cnn.h5")
# loaded_model.load_weights("mnist_cnn_model_desktop.h5")
loaded_model.load_weights("mnist_cnn_model.h5")

loaded_model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
prediction = loaded_model.predict(x)
# prediction = np_utils.categorical_probas_to_classes(prediction)
prediction = np.argmax(prediction, axis=1)
print(prediction)
gc.collect()