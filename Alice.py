from collections import Counter
import re
import time
from datetime import datetime



input_string = open('Alice.txt')
input_string = input_string.read()
# input_string = "Hello I am going to I with hello am"

words = input_string.split()

count = 0
# start = time.time()
# start = time.process_time()
start = time.perf_counter()
for x in words:
    if len(x) > 12:
        count+=1
# stop = time.time()
# stop = time.process_time()
stop = time.perf_counter()

print(count)

diff = stop - start
print(diff * 1000000000)